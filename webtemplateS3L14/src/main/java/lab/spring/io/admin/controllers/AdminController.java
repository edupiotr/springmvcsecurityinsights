package lab.spring.io.admin.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import lab.spring.io.core.service.CustomerService;

@Controller
public class AdminController {

	@Autowired
	private CustomerService customerService;
	
	
	@ResponseBody
	@RequestMapping(value = {"/", "/admin"})
	public String home() {
		return "Welcome Admin";
	}

	
//	@RequestMapping("/welcome")
//	public String welcome(ModelMap model) {
//		model.addAttribute("name", "PITER");
//		return "welcome";
//	}
	
	
//	@RequestMapping("/welcome")
//	public ModelAndView welcome(@RequestParam(value = "userId", defaultValue = "0")Integer id) {
//		ModelAndView model = new ModelAndView("welcome");
//		if(id!=null && id==1) {
//			model.addObject("name", "PITER");	
//		} else {
//			model.addObject("name", "Anonymous");
//		}
//		
//		return model;
//	}
	
	
	@RequestMapping("/welcome/{userId}")
	public ModelAndView welcome(@PathVariable(value = "userId")Integer id) {
		ModelAndView model = new ModelAndView("welcome");
//		if(id!=null && id==1) {
//			model.addObject("name", "PITER");	
//		} else {
//			model.addObject("name", "Anonymous");
//		}
		model.addObject("name", customerService.findNameById(id));
		return model;
	}
}
